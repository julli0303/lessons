# Реализовать интерфейс класса Car.
import abc
class ABCCar(abc.ABC):
    @abc.abstractmethod
    def speed(self):
        pass
    @abc.abstractmethod
    def weight(self):
        pass
    @abc.abstractmethod
    def color(self):
        pass

class Car(ABCCar):
    def __init__(self, make="mercedez", model="C200", year="2016", speed=0):
        self._make = make
        self._model = model
        self._year = year
        self._speed = speed
    def get_descriptive_name(self):
        long_name = str(self._year)+''+self._make+''+self._model
        return long_name.title()





