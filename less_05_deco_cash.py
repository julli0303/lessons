# Реализовать декоратор кэширования вызова функции
# В случае, если вызывается функция c одинаковыми
# параметрами, то результат не должен заново вычисляться,
# а возвращаться из хранилища
#
# * изучить lru_cache.

from functools import lru_cache

@lru_cache
def fib(n):
    if n < 2:
        return n
    return fib(n-1) + fib(n-2)
print(fib(50))
print(fib.cache_info())
print (type (lru_cache()))














