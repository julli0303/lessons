# Реализовать класс-миксин, добавляющий классу Car атрибут
# spoiler.
# Spoiler должен влиять на Car.speed , увеличивая ее на значение N.
#
#
# Функция принимает список имен.
# Функция возвращает список отфильтрованных имен по переданному литералу
#     :param
#         names: список имен
#         literal: буква фильтрации. Заглавный или строчный - не имеет значение.
#
#     :return:  список отфильтрованных имен. В ответе все имена должны начинаться с заглавной буквы и быть отсортированы
#
#     Example:
#     get_names_startswith_literal(['adam', 'Bob', 'Adrian'], 'a') == ['Adam', 'Adrian'].
import abc
class ABCCar(abc.ABC):
    @abc.abstractmethod
    def speed(self):
        pass
    @abc.abstractmethod
    def weight(self):
        pass

class Car(ABCCar):
    def __init__(self, make, model, year):
        self._make = make
        self._model = model
        self._year = year


    def get_speed(self):
        return self._speed + 10

    def stop(self):
        if self.get_speed() != 0:
            """плавно уменьшаем скорость"""



class Spoiler:
    def get_speed(self):
        self._speed += 1
        return self._speed


class MyCar(Spoiler, Car):
    def __init__(self, speed_increase):
        self.speed()

        

    def new_get_speed(self):
        res = self.get_speed()
        return res


names = ['Anna', 'Ara', 'Bob', 'Nick', 'lola', 'djeck']
def names_sorted():
    return names.sort()
names_sorted()
print("Ordered list:", names)







